package com.franzliszt.foodturntable;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.franzliszt.SP;

public class MainActivity extends AppCompatActivity {
    private AnimatedCircleLoadingView loadingView;
    private Turntable turntable;
    private int count = 0;
    private ImageView ChangeStatus;
    private RadioGroup RG;
    private SP sp;
    private Context context = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility( View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE );
            getWindow().setStatusBarColor( Color.TRANSPARENT );
        }
        setContentView( R.layout.activity_main );
        InitView();
        SelectNumber();
    }

    private void InitView() {
        turntable = findViewById( R.id.TurnTable );
        loadingView = findViewById( R.id.loadingView );
        ChangeStatus = findViewById( R.id.StartAndEnd );
        RG = findViewById( R.id.RG );
        /*默认设置8等份*/
        turntable.InitNumber( 8 );
        if (context == null) {
            context = MainActivity.this;
        }
        sp = new SP( context );
    }

    public void Start(View view) {
        count++;
        /*暂停*/
        if (count % 2 == 0) {
            turntable.Stop();
            StartIcon();
        } else {
            /*开始*/
            turntable.Start( -1 );
            StopIcon();
        }
    }

    private void StartIcon() {
        ChangeStatus.setImageDrawable( getResources().getDrawable( R.drawable.start ) );
    }

    private void StopIcon() {
        ChangeStatus.setImageDrawable( getResources().getDrawable( R.drawable.stop ) );
    }

    public void Confirm(View view) {
        RevealAnim( view );
        loadingView.setVisibility( View.VISIBLE );
        startLoading();
        startPercentMockThread();
        int num = (int) sp.GetData( context, "num", 0 );
        turntable.InitNumber( num );
    }

    public void Reset(View view) {
        RevealAnim( view );
        loadingView.setVisibility( View.GONE );
        resetLoading();
    }

    private void startLoading() {
        loadingView.startIndeterminate();
    }

    private void startPercentMockThread() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep( 500 );
                    for (int i = 0; i <= 100; i++) {
                        Thread.sleep( 40 );
                        changePercent( i );
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(runnable).start();
    }

    private void changePercent(final int percent) {
        runOnUiThread( new Runnable() {
            @Override
            public void run() {
                loadingView.setPercent( percent );
            }
        } );
    }

    public void resetLoading() {
        runOnUiThread( new Runnable() {
            @Override
            public void run() {
                loadingView.resetLoading();
            }
        } );
    }

    private void RevealAnim(View view) {
        Animator animator = ViewAnimationUtils.createCircularReveal(
                view, view.getWidth() / 2, view.getHeight() / 2, view.getWidth(), 0
        );
        animator.setInterpolator( new AccelerateDecelerateInterpolator() );
        animator.setDuration( 2000 );
        animator.start();

    }

    private void SelectNumber() {
        RG.setOnCheckedChangeListener( new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.fourParts:
                        sp.PutData( context, "num", 4 );
                        break;
                    case R.id.sixParts:
                        sp.PutData( context, "num", 6 );
                        break;
                    case R.id.eightParts:
                        sp.PutData( context, "num", 8 );
                        break;
                }
            }
        } );
    }
}
