package com.franzliszt.foodturntable;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.SurfaceHolder;
import android.view.SurfaceView;



public class Turntable extends SurfaceView implements SurfaceHolder.Callback, Runnable {

    private SurfaceHolder mSurfaceHolder;
    private Canvas mCanvas;
    /**
     * 用于SurfaceView绘制的子线程
     */
    private Thread mThread;
    /**
     * 控制子线程开关
     */
    private boolean isRunning;
    /**
     * 字样*/
    private String[] itemCharString;
    /**
     * 图片*/
    private int[] itemImages;
    private Bitmap[] mBitmaps;

    /**
     * 背景
     */
    private Bitmap mBgBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.turntable_bgcolor );
   /**
    * 色块*/
    private int[] itemColors;
   /**
    * 默认等份*/
    private int mItemCount = 8;

    /**
     * 整个盘块的范围
     */
    private RectF mRange = new RectF();
    /**
     * 整个盘块的直径
     */
    private int mRadius;
    /**
     * 绘制盘块的画笔
     */
    private Paint mArcPaint;
    /**
     * 绘制文本的画笔
     */
    private Paint mTextPaint;
    /**
     * 字体大小*/
    private float mTextSize = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_SP, 20, getResources().getDisplayMetrics());
    /**
     * 盘块滚动的速度
     */
    private double mSpeed = 0;

    /**
     * 转盘的中心位置
     */
    private int mCenter;
    /**
     * 这里我们的padding直接取paddingLeft
     */
    private int mPadding;

    /**
     * volatile保证线程间的可见性
     */
    private volatile float mStartAngle = 0;

    /**
     * 判断是否点击了停止按钮
     */
    private boolean isShouldEnd = false;

    /**
     * 设置单次绘制最低时间，如果在该时间内绘制完成，让子线程sleep到改时间结束
     * 这样防止了线程绘制频繁，先消耗性能的问题
     */
    private long mOneTimeMinMillionSeconds = 50;

    private int mDifferSpeed = 1;// 调用停止后递减的速度差值 要大于0

    public Turntable(Context context) {
        super(context);
        init();
    }

    public Turntable(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Turntable(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    public void InitNumber(int number){
        if (number <= 0){
            return;
        }
        /**
         * 确保为偶数*/
        if (number % 2 == 0){
            InitArea(number);
        }
    }
    private void InitArea(int number){
        switch (number){
            case 4:
                fourParts();
                break;
            case 6:
                sixParts();
                break;
            case 8:
                eightParts();
                break;
                default:
                    sixParts();
        }
    }
    private void fourParts(){
        mItemCount = 4;
        itemCharString = new String[]{"粉条", "面条", "米饭", "粥",};
        itemImages = new int[]{R.drawable.fen, R.drawable.mian, R.drawable.rice, R.drawable.tang};
        itemColors = new int[]{0xffffc300, 0xfff17e01, 0xffffc300, 0xfff17e01};
        InitImage(mItemCount,itemImages);
    }
    private void sixParts(){
        mItemCount = 6;
        itemCharString = new String[]{"火锅", "汉堡", "巧克力", "奶茶", "蛋糕", "炸鸡"};
        itemImages = new int[]{R.drawable.huoguo, R.drawable.hanbao, R.drawable.qiaokeli, R.drawable.naicha, R.drawable.dangao, R.drawable.zhaji1};
        itemColors = new int[]{0xffffc300, 0xfff17e01, 0xffffc300, 0xfff17e01, 0xffffc300, 0xfff17e01};
        InitImage(mItemCount,itemImages);
    }
    private void eightParts(){
        mItemCount = 8;
        itemCharString = new String[]{"苹果", "香蕉", "榴莲", "西瓜", "葡萄", "火龙果","芒果","草莓"};
        itemImages = new int[]{R.drawable.apple, R.drawable.xaingjiao, R.drawable.liulian, R.drawable.xigua, R.drawable.putao, R.drawable.huolongguo,R.drawable.mangguo,R.drawable.caomei};
        itemColors = new int[]{0xffffc300, 0xfff17e01, 0xffffc300, 0xfff17e01, 0xffffc300, 0xfff17e01,0xffffc300,0xfff17e01};
        InitImage(mItemCount,itemImages);
    }
    private void init() {
        mSurfaceHolder = this.getHolder();
        // 管理SurfaceView的生命周期
        mSurfaceHolder.addCallback(this);
        // 能够获取焦点
        this.setFocusable(true);
        this.setFocusableInTouchMode(true);
        // 保持常亮
        this.setKeepScreenOn(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = Math.min(getMeasuredWidth(), getMeasuredHeight());
        mPadding = getPaddingLeft();
        mRadius = width - mPadding * 2;
        // 中心点
        mCenter = width / 2;
        setMeasuredDimension(width, width);
    }
    private void InitImage(int count,int[] item){
        mBitmaps = new Bitmap[mItemCount];
        for (int i = 0; i < count; i++) {
            mBitmaps[i] = BitmapFactory.decodeResource(getResources(), item[i]);
        }
    }
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        // 初始化盘块画笔
        mArcPaint = new Paint();
        mArcPaint.setAntiAlias(true);
        mArcPaint.setDither(true);
        // 初始化文字画笔
        mTextPaint = new Paint();
        mTextPaint.setColor(0xffffffff);
        mTextPaint.setTextSize(mTextSize);
        // 初始化盘块绘制范围
        mRange = new RectF(mPadding, mPadding, mPadding + mRadius, mPadding + mRadius);
        // 初始化图片
        InitImage(mItemCount,itemImages);
        isRunning = true;
        mThread = new Thread(this);
        mThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        isRunning = false;
    }

    @Override
    public void run() {
        /**
         * 不断的进行绘制
         */
        while (isRunning) {
            long preMillions = System.currentTimeMillis();
            draw();
            long afterMillions = System.currentTimeMillis();
            long drawOnceTime = afterMillions - preMillions;
            if (drawOnceTime < mOneTimeMinMillionSeconds) {
                try {
                    Thread.sleep(mOneTimeMinMillionSeconds - drawOnceTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void draw() {
        try {
            mCanvas = mSurfaceHolder.lockCanvas();
            if (mCanvas != null) {
               /* 绘制背景颜色*/
                drawBgColor();
                /*绘制区域*/
                drawArea();
            }
        } catch (Exception e) {
           e.printStackTrace();
        } finally {
            if (mCanvas != null) {
                mSurfaceHolder.unlockCanvasAndPost(mCanvas);
            }
        }
    }

    /**
     * 绘制盘块
     */
    private void drawArea() {
        // 起始角度
        float tempAngle = mStartAngle;
        // 每个盘块绘制的角度
        float sweepAngele = 360 / mItemCount;
        for (int i = 0; i < mItemCount; i++) {
            mArcPaint.setColor(itemColors[i]);
            // 绘制盘块
            mCanvas.drawArc(mRange, tempAngle, sweepAngele, true, mArcPaint);
            // 绘制文本
            drawText(tempAngle, sweepAngele, itemCharString[i]);
            // 绘制图标
            drawIcon(tempAngle, mBitmaps[i]);

            tempAngle += sweepAngele;
        }
        mStartAngle += mSpeed;

        // 如果需要停止,让转速逐渐变小直到0
        if (isShouldEnd) {
            mSpeed -= mDifferSpeed;
        }
        if (mSpeed <= 0) {
            mSpeed = 0;
            isShouldEnd = false;
        }
    }

    /**
     * 绘制每个盘块的图标
     *
     * @param tempAngle
     * @param bitmap
     */
    private void drawIcon(float tempAngle, Bitmap bitmap) {
        // 约束图片的宽度，为直径的1/8,可以作为可变参数设置
        int imgWidth = mRadius / 8;
        // 获取弧度值
        float angle = (float) ((tempAngle + 360 / mItemCount / 2) * Math.PI / 180);
        // 约定图片位置在直径1/4处
        int x = (int) (mCenter + mRadius / 4 * Math.cos(angle));
        int y = (int) (mCenter + mRadius / 4 * Math.sin(angle));
        // 确定图片位置
        Rect rect = new Rect(x - imgWidth / 2, y - imgWidth / 2, x + imgWidth / 2, y + imgWidth / 2);
        mCanvas.drawBitmap(bitmap, null, rect, null);
    }

    /**
     * 绘制每个盘块的文本
     *
     * @param tempAngle
     * @param sweepAngele
     * @param itemTextStr
     */
    private void drawText(float tempAngle, float sweepAngele, String itemTextStr) {
        Path path = new Path();
        path.addArc(mRange, tempAngle, sweepAngele);
        // 利用水平偏移量让文字居中
        float textWidth = mTextPaint.measureText(itemTextStr);
        int hOffset = (int) (mRadius * Math.PI / mItemCount / 2 - textWidth / 2);
        // 利用垂直偏移量让文字向圆心靠拢
        int vOffset = mRadius / 2 / 6;
        mCanvas.drawTextOnPath(itemTextStr, path, hOffset, vOffset, mTextPaint);
    }

    /**
     * 绘制背景
     */
    private void drawBgColor() {
        mCanvas.drawColor(0xFFFFFFFF);
        mCanvas.drawBitmap(mBgBitmap, null, new Rect(mPadding / 2, mPadding / 2, getMeasuredWidth() - mPadding / 2, getMeasuredWidth() - mPadding / 2), null);
    }

    /**
     * 启动转盘
     * 能够控制到具体某个index范围内停止
     */
    public void Start(int index) {
        if (isStart()) {
            return;
        }
        if (index < 0) {
            mSpeed = 50 * (1 + Math.random() * (0.5));
            isShouldEnd = false;
            return;
        }
    }

    /**
     * 停止转盘
     */
    public void Stop() {
        if (isShouldEnd()) {
            return;
        }
        // 将初始角度重置
        mStartAngle = 0;
        isShouldEnd = true;
    }

    /**
     * 转盘是否在旋转
     *
     * @return
     */
    public boolean isStart() {
        return mSpeed != 0;
    }

    /**
     * 是否停止状态(但可能处于旋转减速到停止)
     *
     * @return
     */
    public boolean isShouldEnd() {
        return isShouldEnd;
    }
}
