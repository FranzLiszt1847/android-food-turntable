# Android美食大转盘

#### 介绍
利用SurfaceView绘制美食大转盘，提供RadioButton与用户进行交互，根据需求将转盘分为几等份，本示例提供4，6，8三中不同的等份，分别对应主食，外卖，水果三个系列

#### 效果视频
一、整体效果视频 :heartbeat: 

![全部效果](https://images.gitee.com/uploads/images/2021/1112/161212_c745b09d_8759392.gif "Samsung Galaxy S10 (1440x3040, 560dpi) - 192.168.222.101 - Genymotion 2021-11-12 13-27-54 (2).gif")

二、按钮Reveal动画:heartbeat: 

![按钮Reveal动画](https://images.gitee.com/uploads/images/2021/1112/161523_2f4ba36f_8759392.gif "Samsung Galaxy S10 (1440x3040, 560dpi) - 192.168.222.101 - Genymotion 2021-11-12 13-27-54 (1).gif")

#### 效果图

 ![布局截图](https://images.gitee.com/uploads/images/2021/1112/161309_f2116b21_8759392.png "屏幕截图.png")
 ![8等份](https://images.gitee.com/uploads/images/2021/1112/161350_55ec1768_8759392.png "屏幕截图.png")
 ![6等份](https://images.gitee.com/uploads/images/2021/1112/161418_16089e8c_8759392.png "屏幕截图.png")
 ![4等份](https://images.gitee.com/uploads/images/2021/1112/161445_37873134_8759392.png "屏幕截图.png")

#### 代码解析

1.  配置转盘等份

```
 turntable.InitNumber( 8 );
```

2.  开始旋转转盘和暂停旋转转盘

```
public void Start(View view) {
        count++;
        /*暂停*/
        if (count % 2 == 0) {
            turntable.Stop();
            StartIcon();
        } else {
            /*开始*/
            turntable.Start( -1 );
            StopIcon();
        }
    }
```

3.  RevealAnimtoar

```
private void RevealAnim(View view) {
        Animator animator = ViewAnimationUtils.createCircularReveal(
                view, view.getWidth() / 2, view.getHeight() / 2, view.getWidth(), 0
        );
        animator.setInterpolator( new AccelerateDecelerateInterpolator() );
        animator.setDuration( 2000 );
        animator.start();

    }
```
4.XML的使用

```
<com.franzliszt.foodturntable.Turntable
        android:id="@+id/TurnTable"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:padding="20dp"
        android:layout_margin="10dp"
        android:layout_centerInParent="true"/>
```




#### CSDN博文地址
[博文地址](https://blog.csdn.net/News53231323/article/details/121287487)


